from django.db import models
from django.contrib.postgres.fields import JSONField


class Movie(models.Model):
    title = models.CharField(max_length=500, unique=True)
    details = JSONField(default=dict)

    def __str__(self):
        return f'[{self.pk}] - {self.title}'
