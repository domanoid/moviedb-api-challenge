from django.db import models
from django.db.models import Window, F, Q
from django.db.models.functions import DenseRank
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from movies.models import Movie

from utils import omdb_api


@csrf_exempt
def movies(request):
    if request.method == "POST":
        title = request.body.decode("utf8")
        if len(title) == 0:
            return JsonResponse({"error": "Please specify exact movie title!"}, status=409)

        if Movie.objects.filter(title__iexact=title).exists():
            return JsonResponse({"error": "This movie already exists!"}, status=409)

        movie = Movie(title=title)
        details = omdb_api.get_movie_details(movie.title)
        if details is None:
            return JsonResponse({"error": "Movie database error!"}, status=503)

        if 'Error' in details:
            return JsonResponse({"error": "Movie not found in database!"}, status=404)

        movie.details = details
        movie.save()
        return JsonResponse({"id": movie.pk, **movie.details})

    movies_qs = Movie.objects.all()
    if "title" in request.GET:
        movies_qs = movies_qs.filter(title=request.GET.get("title"))
    if "year" in request.GET:
        movies_qs = movies_qs.filter(details__Year=request.GET.get("year"))

    if "sort" in request.GET:
        sort_by = request.GET.get("sort")
        if sort_by in ["Title", "Year", "imdbRating"]:
            movies_qs = movies_qs.order_by(f'details__{sort_by}')
        else:
            return JsonResponse({"error": "You can sort only by: Title, Year and imdbRating"}, status=400)

    response = [
        {
            "id": movie.pk,
            **movie.details
        } for movie in movies_qs
    ]
    return JsonResponse(response, safe=False)


def top(request):
    q_objects = Q()
    if "start" in request.GET:
        q_objects.add(Q(comments__created_at__gte=request.GET.get("start")), Q.AND)
    if "end" in request.GET:
        q_objects.add(Q(comments__created_at__lte=request.GET.get("end")), Q.AND)

    movies_qs = Movie.objects.filter(q_objects)
    movies_with_ranks = (
        movies_qs
            .annotate(total_comments=models.Count('comments'))
            .annotate(rank=Window(expression=DenseRank(),
                                  order_by=F('total_comments').desc()))
            .order_by("rank", "pk")
            .values("pk", "rank", "total_comments")
    )
    response = [
        {
            "movie_id":       m["pk"],
            "rank":           m["rank"],
            "total_comments": m["total_comments"],
        } for m in movies_with_ranks
    ]
    return JsonResponse(response, safe=False)
