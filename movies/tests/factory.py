import factory

from movies.models import Movie


class MovieFactory(factory.DjangoModelFactory):
    class Meta:
        model = Movie

    title = factory.Faker('sentence', nb_words=4, variable_nb_words=True)
