from django.db import models


class Comment(models.Model):
    movie = models.ForeignKey('movies.Movie', on_delete=models.CASCADE, related_name='comments')
    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'[{self.pk}] -> Movie: {self.movie} Comment: {self.comment}'
