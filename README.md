# MOVIE DB API BACKEND
Simple REST API application written in Django.

It is a movie database with comments.  
A user can add a movie by its title. All details will be fetched from omdbapi.  
After movie is added user can add comments to it.  
Everyone can check top movies by date range (sorted by comments count).


## Endpoints 

### `/movies`  
`POST`
- Request body should contain only movie title, and its presence should be validated.
- Based on passed title, other movie details should be fetched from http://www.omdbapi.com/ (or other similar, public movie database) - and saved to application database.
- Request response should include full movie object, along with all data fetched from external API.

```bash
curl --request POST \
  --url http://127.0.0.1:8000/movies \
  --data Terminator
```

`GET`
- Should fetch list of all movies already present in application database.
- Additional filtering, sorting is fully optional - but some implementation is a bonus.

Sorting is done by setting request query parameter `sort` to `Title`, `Year` or `imdbRating`.  
Filtering is done by setting request query parameter `title` or `year`.

```bash
curl --request GET \
  --url 'http://127.0.0.1:8000/movies?sort=Year'

curl --request GET \
  --url 'http://127.0.0.1:8000/movies?year=1991&sort=Title'
```

### `/comments`
`POST`
- Request body should contain ID of movie already present in database, and comment text body.
- Comment should be saved to application database and returned in request response.

```bash
curl --request POST \
  --url http://127.0.0.1:8000/comments \
  --header 'content-type: multipart/form-data' \
  --form movie=5 \
  --form 'comment=This is really good movie!'
```

`GET`
- Should fetch list of all comments present in application database.
- Should allow filtering comments by associated movie, by passing its ID.

```bash
curl --request POST \
  --url http://127.0.0.1:8000/comments

curl --request GET \
  --url 'http://127.0.0.1:8000/comments?movie=5' \
  --header 'content-type: multipart/form-data'
```

### `/top`
`GET`
- Should return top movies already present in the database ranking based on a number of comments added to the movie (as in the example) in the specified date range. The response should include the ID of the movie, position in rank and total number of comments (in the specified date range).
- Movies with the same number of comments should have the same position in the ranking.
- Should require specifying a date range for which statistics should be generated. 

Supports data ranges with query parameters `start` and `end`.

```bash
curl --request GET \
  --url 'http://127.0.0.1:8000/top?start=2019-01-01%2012%3A00%3A00&end=2019-10-01%2012%3A00%3A00'

curl --request GET \
  --url 'http://127.0.0.1:8000/top?start=2019-01-01&end=2019-10-01'
```

## Requirements
- Production:  
`pip install -r requirements.txt`  
- Development:  
`pip install -r requirements/dev.txt`  


## Running locally
```bash
docker-compose build
docker-compose up
docker-compose run --rm django migrate
```

Navigate to `127.0.0.1:8000/<endpoint_url>`

## Testing
Run commands in section `Running locally` and then:  
`docker-compose run --rm django pytest -vv`