from pprint import pprint
from typing import Union

import requests

# TODO: move to settings
API_KEY = "fb54e1b4"
API_URL = "http://www.omdbapi.com/"

payload = {
    "apikey": API_KEY
}


def get_movie_details(movie_title: str, timeout: int = 2) -> Union[dict, None]:
    payload.update({
        "t": movie_title
    })

    try:
        response = requests.get(API_URL, payload, timeout=timeout)
    except requests.exceptions.Timeout:
        # TODO: reporting?
        return None

    if response.status_code != 200:
        # TODO: reporting?
        return None

    try:
        movie_details = response.json()
    except:
        # TODO: reporting?
        return None

    return movie_details
